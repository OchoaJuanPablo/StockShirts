package com.OchoaJuanPablo.ActuarioDeCamisas.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.OchoaJuanPablo.ActuarioDeCamisas.Dtos.dao.IOperatorDao;
import com.OchoaJuanPablo.ActuarioDeCamisas.exceptions.ResourceRepeatModelDtoException;
import com.OchoaJuanPablo.ActuarioDeCamisas.models.Operator;
@RestController
@RequestMapping("/inventory_operator")
public class OperatorController {
	private static final Logger log = LoggerFactory.getLogger(ActuarioController.class);
	@Autowired
	private IOperatorDao iOperatorDao;
	
	//OPERATOR RESTFUL ----> METHODS GET , POST , PUT AND DELETE
	
	@GetMapping("/operator_listAll")
	public ArrayList<Operator> listAllOperators(Operator operator){
		return (ArrayList<Operator>) iOperatorDao.findAll();
	}
	
	@GetMapping("/operator/{id}")
	public ResponseEntity<Operator> listOperatorById(@PathVariable String id){
		log.info("search operator by id"+id);
		try {
			Operator operator = iOperatorDao.findById(id).orElseThrow(()-> new ResourceRepeatModelDtoException("doesnt exist operator : "+id));
		    return ResponseEntity.ok(operator);
		}catch(Exception exception) {
			log.error("Error to search by id at operator: "+id);
			exception.getStackTrace();
			return new ResponseEntity<Operator>(HttpStatus.BAD_REQUEST);
		}
	}

	
	@PostMapping("/operators_save")
	public ResponseEntity<String> createOperator(@RequestBody Operator operator){
		log.info("Creating operator "+operator);
		try {
			iOperatorDao.save(operator);
			return ResponseEntity.status(200).body("Saved operator");
		}catch(Exception exception) {
			log.error("Error to create operator : "+exception.getMessage());
			exception.getStackTrace();
			return ResponseEntity.status(500).body(exception.getMessage());
		}
	}
	
	@PutMapping("/operator_update/{employee_id}")
	public ResponseEntity<Operator> updateOperatroById(@PathVariable String employee_id , @RequestBody Operator ChangesOfOperator){
		log.info("updating shirt by id : "+employee_id);
		try {
			Optional<Operator> operatorOptional=iOperatorDao.findById(employee_id);
			if(operatorOptional.isPresent()) {
				//operatorOptional.get().setEmployeeId(ChangesOfOperator.getEmployeeId());
				operatorOptional.get().setName(ChangesOfOperator.getName());
			}
			Operator operatorUpdate = iOperatorDao.save(operatorOptional.get());
			return ResponseEntity.ok(operatorUpdate);
		}catch(Exception exception) {
			log.error("Error to updating operator : "+exception.getMessage(),ChangesOfOperator);
			return new ResponseEntity<Operator>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/operator_delete/{id}")
	public ResponseEntity<String> deleteOperatorById (@PathVariable String id){
		log.info("delete operator by id : "+id);
		try {
			Operator operator = iOperatorDao.findById(id).orElseThrow(() -> new ResourceRepeatModelDtoException("Doesnt exist operator" + id));
			iOperatorDao.delete(operator);
			return ResponseEntity.status(200).body("deleted operator");
		}catch(Exception exception) {
			log.error("Error to delete operator : "+exception.getMessage(),id);
			exception.getStackTrace();
			return ResponseEntity.status(500).body(exception.getMessage());
		}
	}
	
}
