package com.OchoaJuanPablo.ActuarioDeCamisas.controllers;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.OchoaJuanPablo.ActuarioDeCamisas.Dtos.dao.IOperatorDao;
import com.OchoaJuanPablo.ActuarioDeCamisas.Dtos.dao.IShirtDao;
import com.OchoaJuanPablo.ActuarioDeCamisas.exceptions.ResourceRepeatModelDtoException;
import com.OchoaJuanPablo.ActuarioDeCamisas.models.Operator;
import com.OchoaJuanPablo.ActuarioDeCamisas.models.Shirt;


@RestController
@RequestMapping("/inventory_shirts")
public class ActuarioController {
	private static final Logger log = LoggerFactory.getLogger(ActuarioController.class);
	@Autowired
	private IShirtDao ishirtDao;
	
	// SHIRT RESTFUL METHOD GET,POST,PUT AND DELETE . ALL WORKS ! 
	
	@GetMapping("/shirts_listAll")
	public ArrayList<Shirt> listAllShirts(Shirt shirt) {
		return (ArrayList<Shirt>) ishirtDao.findAll();
	}
	
	@GetMapping("/shirts/{id}")
	public ResponseEntity<Shirt> listShirtById(@PathVariable String id) {
		log.info("search shirt by id"+id);
		try {
			Shirt shirt = ishirtDao.findById(id).orElseThrow(() -> new ResourceRepeatModelDtoException("Doesnt exist shirt" + id));
			return ResponseEntity.ok(shirt);
		} catch (Exception exception) {
			log.error("Error to search by id at shirt"+id);
			exception.getStackTrace();
			return new ResponseEntity<Shirt>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/shirt_save")
	public ResponseEntity<String> createShirt(@RequestBody Shirt shirt){
		log.info("Saving new shirt : "+shirt);
		try {
			ishirtDao.save(shirt);
			return ResponseEntity.status(200).body("saved");
		}catch(Exception exception) {
			log.error("Error while trying to save new banner: {}, with message:"+exception.getMessage(),shirt);
			return ResponseEntity.status(500).body(exception.getMessage());
		}
	}

	@PutMapping("/shirt_update/{id_shirt}")
	public ResponseEntity<String> searchShirtById(@PathVariable String id_shirt,@RequestBody Shirt shirt){
		log.info("search shirt by id : "+id_shirt);
		try {
			Optional<Shirt> shirtOptional=ishirtDao.findById(id_shirt);
			if(shirtOptional.isPresent()) {
				shirtOptional.get().setStock(shirt.getStock());
			}
			Shirt shirtUpdate = ishirtDao.save(shirtOptional.get());
			return ResponseEntity.status(200).body("Shirt Updated");
		}catch(Exception exception) {
			log.error("Error to update stock shirt"+exception.getMessage());
			exception.getStackTrace();
			return ResponseEntity.status(500).body(exception.getMessage());
		}
	}
	@DeleteMapping("/delete_shirt/{id}")
	public ResponseEntity<String> deleteShirtById (@PathVariable String id){
		log.info("delete shirt by id"+id);
		try {
			Shirt shirt = ishirtDao.findById(id).orElseThrow(() -> new ResourceRepeatModelDtoException("Doesnt exist shirt" + id));
			ishirtDao.delete(shirt);
			return ResponseEntity.status(200).body("deleted shirt");
		}catch(Exception exception) {
			log.error("Error to delete shirt : "+exception.getMessage(),id);
			exception.getStackTrace();
			return ResponseEntity.status(500).body(exception.getMessage());
		}
	}
	
}
