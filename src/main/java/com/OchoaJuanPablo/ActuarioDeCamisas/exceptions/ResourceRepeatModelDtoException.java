package com.OchoaJuanPablo.ActuarioDeCamisas.exceptions;

public class ResourceRepeatModelDtoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ResourceRepeatModelDtoException(String message) {
		super (message);
	}

}
