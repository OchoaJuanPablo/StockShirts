package com.OchoaJuanPablo.ActuarioDeCamisas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuarioDeCamisasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuarioDeCamisasApplication.class, args);
	}

}
