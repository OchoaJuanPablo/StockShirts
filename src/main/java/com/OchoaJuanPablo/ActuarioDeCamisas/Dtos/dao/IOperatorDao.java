package com.OchoaJuanPablo.ActuarioDeCamisas.Dtos.dao;

import org.springframework.data.repository.CrudRepository;

import com.OchoaJuanPablo.ActuarioDeCamisas.models.Operator;

public interface IOperatorDao extends CrudRepository<Operator, String> {

}
