package com.OchoaJuanPablo.ActuarioDeCamisas.Dtos.dao;

import org.springframework.data.repository.CrudRepository;

import com.OchoaJuanPablo.ActuarioDeCamisas.models.Shirt;

public interface IShirtDao extends CrudRepository<Shirt, String> {
	
}
