package com.OchoaJuanPablo.ActuarioDeCamisas.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="operator_table")
public class Operator implements Serializable {
	private static final long serialVersionUID = 8246822906238835033L;
	
	@Id
	@Column(name = "employee_id" , length=60)
	private String employeeId;
	@Column(name = "name_employee",length = 100)
	private String name;
	public Operator() {
		
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
