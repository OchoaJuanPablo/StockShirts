package com.OchoaJuanPablo.ActuarioDeCamisas.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="shirt_table")
public class Shirt implements Serializable {
	private static final long serialVersionUID = -6902588050792331453L;
	@Id
	@Column(name = "id_Shirt", length=40)
	private String id_shirt;
	@Column(name = "product_name" , length=50)
	private String productName;
	@Column(name= "stock")
	private Long stock;
	@Column(name = "descritpion" , length=200)
	private String description;
	
	public Shirt() {
		
	}

	public String getId() {
		return id_shirt;
	}

	public void setId(String id_shirt) {
		this.id_shirt = id_shirt;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getStock() {
		return stock;
	}

	public void setStock(Long stock) {
		this.stock = stock;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
